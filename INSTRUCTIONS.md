## Trial Identification Kata

At Deep Lens, part of what we do is identify patients that may qualify for a clinical trial. This identification is made based on numerous criteria ranging from age, which is fairly easily to determine programmatically, to primary diagnosis, which can be more challenging.

Your task is to build a similar system, given a known input, and a series of acceptance criteria.

In the data folder there are two files, `clinical_trial.csv` and `patient_feed.csv`. The clinical trial csv contains the information for single clinical trial you will be identifying patients for. The patient feed csv contains a list of potential patient matches for the clinical trial. These two files must be read in at the start of your program.

As reviewers of your kata we have some basic assumptions for how the kata is completed. 

- This kata is language agnostic as long as you can effectively complete a program that fulfills the acceptance criteria
- There should be a single entry point to the program that can run in a single command to produce the output expected by the acceptance criteria
- The program should be idempotent, if it is run multiple times in a row, the output should be exactly the same
- The input and output of the program is text, so there is not an expectation of a graphical user interface
- Please feel free to email us questions about the acceptance criteria!

### Acceptance Criteria

**Given I am the Deep Lens system**  
**When I run the trial identification program**  
**Then I should see an output containing information about the trial(s) being identified followed by the number of patients that are being run through the process**      

The output should look like

```
Processing trial id NCT03840902 for condition Non-small Cell Lung Cancer with 8 potential patients
```

--

**Given I am the Deep Lens system**  
**When I run the trial identification program**  
**Then I should see, for each patient, output indicating the patient being processed for the trial**

```
-- Processing patient \<patient_id>, age \<age>, gender \<gender>, with diagnosis \<diagnosis>, for trial NCT03840902
```

--

**Given I am the Deep Lens system processing an individual patient for trial identification**  
**Then I should ensure that the patient age is correct based on the trial's age criteria (greater than 18)**

Output for age that meets the trial criteria should look like:

```
 -- Patient \<patient_id> meets age criteria at \<age> years old
```

Output for age that does not meet the trial criteria should look like:

```
 -- Patient \<patient_id> does not meet age criteria at \<age> years old
```

--

**Given I am the Deep Lens system processing an individual patient for trial identification**   
**Then I should ensure that the diagnosis of the patient contains one of the two types of cancer as defined in the `type` column (pipe delimited) in the clinical trial csv**  

Output for diagnosis that meets the trial type criteria should look like:

```
 -- Patient \<patient_id> meets diagnosis criteria \<type_from_clinical_trial> with diagnosis of \<diagnosis_from_patient_record>
```

Output for diagnosis that does not meet the trial type criteria should look like:

```
 -- Patient \<patient_id> does not meet diagnosis criteria \<type_from_clinical_trial>
```

--

**Given I am the Deep Lens system processing an individual patient for trial identification**  
**Then I should ensure that the anatomic site of the cancer detected matches what is defined in the trial criteria**

Output for anatomic site that meets the trial anatomic site criteria should look like:

```
 -- Patient \<patient_id> meets anatomic site criteria with \<anatomic_site_from_patient_record>
```

Output for the anatomic site that does not meet the trial anatomic site criteria should look like:

```
 -- Patient \<patient_id> does not meet anatomic site criteria of \<anatomic_site_from_clinical_trial>
```

*Note: You can assume that any anatomic site in a patient record that contains the text of the anatomic site in the clinical trial is a match.*

--

**Given I am the Deep Lens system processing an individual patient for trial identification**  
**When I match diagnosis but not anatomic site**  
**Then I do not qualify for the trial**  

--

**Given I am the Deep Lens system processing an individual patient for trial identification**  
**When I match anatomic site but not diagnosis**  
**Then I do not qualify for the trial**  

--

**Given I am the Deep Lens system**    
**When I am processing an individual patient record and I find an eligibility criteria is not met**  
**Then I should not display any more output for that patient record**  

--

**Given I am the Deep Lens system**  
**When I complete processing all of the individual patient records**  
**Then I should output the patients that have successfully been identified as potential matches for the trial**  

Output for the final step should look like the following, containing a list of the eligable patients after the first line:

```
The following \<count> patients are potentially eligible for NCT03840902  
 -- Patient <patient_id>
```

## Submitting your kata!

Congrats, you reached the end of acceptance criteria. To submit your work to Deep Lens, please email either a [git bundle](https://git-scm.com/docs/git-bundle) of your code or a zip file.  

In your submission please include instructions on how to run your program. It is expected that it can be run in a single command from the command line.



