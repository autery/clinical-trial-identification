# Trial::Identification

Curtis's Deep Lens code challenge

Match patients to trials using csv files describing each

## Setup:

Install ruby 2.6.3 and bundler
Run bundler to install project dependencies (factory_bot, rspec, etc.)


## Assumptions:
* There is one trial
* Only one trial type will match a patient diagnosis substring
* Patient data may be incomplete or not use the same case with what is being matched on

## Phase 2 goals:
* Add rails and persistence
* Account for multiple trials
* Clean up validation checks that put to STDOUT and also return a boolean

## Usage

Run bin/identify, include arguments for trial and patients files, e.g.,

    bin/identify data/clinical_trial.csv patient_feed.csv

