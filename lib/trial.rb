module Identification
  class Trial
    attr_reader :id, :title, :description, :phase, :condition, :anatomic_site, :type, :age_requirement

    def initialize row
      @id              = row['trial_id']
      @title           = row['title']
      @description     = row['description']
      @phase           = row['phase']
      @condition       = row['condition']
      @anatomic_site   = row['anatomic_site']
      @type            = row['type']
      @age_requirement = row['age_requirement']
    end

    def age_minimum
      # happy path, assuming all trials will have a >x-years requirement

      age_requirement[1..].to_i + 1
    end

    def types
      type&.split '|'
    end
  end
end
