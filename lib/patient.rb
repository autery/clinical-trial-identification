module Identification
  class Patient
    attr_accessor :id, :age, :gender, :diagnosis, :anatomic_site

    def initialize row
      @id            = row['patient_id']
      @age           = row['age']&.to_i || 0
      @gender        = row['gender'] || 'unknown'
      @diagnosis     = row['diagnosis']
      @anatomic_site = row['anatomic_site'] || 'unknown'
    end
  end
end
