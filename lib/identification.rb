require "trial"
require "patient"

module Identification
  class Processor
    attr_reader :patient, :patients, :trial

    def initialize trial, patients
      @trial = trial
      @patients = patients
    end

    def process
      accepted_patients = []

      puts trial_identifier
      patients.each do |patient|
        @patient = patient
        puts patient_identifier
        accepted_patients << patient if valid?
      end

      puts "The following #{accepted_patients.size} patients are potentially eligible for #{trial.id}"
      accepted_patients.each { |patient| puts "-- Patient #{patient.id}" }
    end

    def trial_identifier
      "Processing trial id #{trial.id} for condition #{trial.condition} with #{patients.length} potential patients"
    end

    def patient_identifier
      "-- Processing patient #{patient.id}, age #{patient.age}, gender #{patient.gender}, with diagnosis #{patient.diagnosis}, for trial #{trial.id}"
    end

    def valid?
      check_age_minimum && check_diagnosis && check_anatomic_site
    end

    def check_age_minimum
      if patient.age >= trial.age_minimum
        puts "-- Patient #{patient.id} meets age criteria at #{patient.age} years old"
        true
      else
        puts "-- Patient #{patient.id} does not meet age criteria at #{patient.age} years old"
        false
      end
    end

    def check_diagnosis
      if trial.types.any? { |type| patient.diagnosis.downcase.include? type }
        puts "-- Patient #{patient.id} meets diagnosis criteria #{trial.type} with diagnosis of #{patient.diagnosis}"
        true
      else
        puts "-- Patient #{patient.id} does not meet diagnosis criteria #{trial.type}"
        false
      end
    end

    def check_anatomic_site
      if patient.anatomic_site.downcase.include? trial.anatomic_site
        puts "-- Patient #{patient.id} meets anatomic site criteria with #{patient.anatomic_site}"
        true
      else
        puts "-- Patient #{patient.id} does not meet anatomic site criteria of #{trial.anatomic_site}"
        false
      end
    end
  end
end
