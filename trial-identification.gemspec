lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "identification"
  spec.version       = "1.0"
  spec.authors       = ["Curtis Autery"]
  spec.email         = ["ceautery@gmail.com"]

  spec.summary       = %q{Match patients to clinical trials}
  spec.description   = %q{Match patients to clinical trials based on age, cancer type, and anatomical site}
  spec.homepage      = "https://gitlab.com/autery/clinical-trial-identification"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "factory_bot", "~> 5.1"
end
