RSpec.describe Identification::Processor do
  def trial_type
    Regexp.quote(trial.type)
  end

  let(:trial) { build :trial }
  let(:patient) { build :patient }

  subject { described_class.new trial, [patient] }

  context 'When I run the trial identification program' do
    it 'output should contain a trial identifier' do
      expect { subject.process }.to output(/Processing trial id NCT03840902/).to_stdout
    end

    it 'output should contain the number of patients' do
      expect { subject.process }.to output(/The following \d+ patients/).to_stdout
    end

    it 'patient output should contain key values and trial identifier' do
      expect { subject.process }.to output(/Processing patient #{patient.id}, age #{patient.age}, gender #{patient.gender}, with diagnosis #{patient.diagnosis}, for trial #{trial.id}/).to_stdout
    end
  end

  context 'Output from processing an individual patient should indicate whether ' do
    it 'patient in correct age range are accepted' do
      patient.age = 19

      expect { subject.process }.to output(/-- Patient #{patient.id} meets age criteria at #{patient.age} years old/).to_stdout
      expect { subject.process }.to output(/-- Patient #{patient.id}$/).to_stdout
    end

    it 'patient not in correct age range are rejected' do
      patient.age = 18

      expect { subject.process }.to output(/-- Patient #{patient.id} does not meet age criteria at #{patient.age} years old/).to_stdout
      expect { subject.process }.to_not output(/-- Patient #{patient.id}$/).to_stdout
    end

    it 'Patients with matched diagnosis are accepted' do
      patient.diagnosis = 'carcinoma'

      expect { subject.process }.to output(/-- Patient #{patient.id} meets diagnosis criteria #{trial_type} with diagnosis of #{patient.diagnosis}/).to_stdout
      expect { subject.process }.to output(/-- Patient #{patient.id}$/).to_stdout
    end

    it 'Patients with unmatched diagnosis are rejected' do
      patient.diagnosis = 'glioblastoma'

      expect { subject.process }.to output(/-- Patient #{patient.id} does not meet diagnosis criteria #{trial_type}/).to_stdout
      expect { subject.process }.to_not output(/-- Patient #{patient.id}$/).to_stdout
    end

    it 'Patients with correct anatomic site are accepted' do
      patient.anatomic_site = 'LUNG'

      expect { subject.process }.to output(/Patient #{patient.id} meets anatomic site criteria with #{patient.anatomic_site}/).to_stdout
      expect { subject.process }.to output(/-- Patient #{patient.id}$/).to_stdout
    end

    it 'Patients with incorrect anatomic site are rejected' do
      patient.anatomic_site = 'brain'

      expect { subject.process }.to output(/Patient #{patient.id} does not meet anatomic site criteria of #{trial.anatomic_site}/).to_stdout
      expect { subject.process }.to_not output(/-- Patient #{patient.id}$/).to_stdout
    end
  end

  context 'Disqualified patients' do
    it 'do not display more output after disqualification is determined' do
      patient.age = 17
      patient.diagnosis = 'glioblastoma'
      patient.anatomic_site = 'brain'

      expect { subject.process }.to output(/-- Patient #{patient.id} does not meet age criteria at #{patient.age} years old/).to_stdout
      expect { subject.process }.to_not output(/-- Patient #{patient.id} does not meet diagnosis criteria #{trial_type}/).to_stdout
      expect { subject.process }.to_not output(/Patient #{patient.id} does not meet anatomic site criteria of #{trial.anatomic_site}/).to_stdout
      expect { subject.process }.to_not output(/-- Patient #{patient.id}$/).to_stdout
    end
  end
end
