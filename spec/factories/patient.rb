FactoryBot.define do
  patient_hash = {
    "patient_id"=>"p-0",
    "age"=>"56",
    "gender"=>"male",
    "diagnosis"=>"adenocarcinoma",
    "anatomic_site"=>"Lung"
  }

  factory :patient, :class =>  Identification::Patient do
    initialize_with { new(patient_hash) }
  end
end
