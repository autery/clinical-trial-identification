FactoryBot.define do
  trial_hash = {
    "trial_id"=>"NCT03840902",
    "title"=>"M7824 With cCRT in Unresectable Stage III Non-small Cell Lung Cancer (NSCLC)",
    "description"=>"M7824 With cCRT in Unresectable Stage III Non-small Cell Lung Cancer (NSCLC),A Multicenter, Double Blind, Randomized, Controlled Study of M7824 With Concurrent Chemoradiation Followed by M7824 Versus Concurrent Chemoradiation Plus Placebo Followed by Durvalumab in Participants With Unresectable Stage III Non-small Cell Lung Cancer",
    "phase"=>"3",
    "condition"=>"Non-small Cell Lung Cancer",
    "anatomic_site"=>"lung",
    "type"=>"adenocarcinoma|carcinoma",
    "age_requirement"=>">18"
  }

  factory :trial, :class =>  Identification::Trial do
    initialize_with { new(trial_hash) }
  end
end
